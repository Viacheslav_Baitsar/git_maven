package com.viach.work;

/**
 * PrintOaaEven.
 */
public class PrintOddEven {
    public boolean odd;
    public int min;
    public int max;

    /**
     * getter.
     *
     * @return odd
     */
    public boolean isOdd() {
        return odd;
    }

    /**
     * @param odd add
     */
    public void setOdd(boolean odd) {
        this.odd = odd;
    }

    /**
     * @param i i.
     * @return min
     */
    public int getMin(int i) {
        return min;
    }

    /**
     * getmin.
     *
     * @param min min
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * grtter.
     *
     * @param i   i
     * @param odd odd
     * @return max
     */
    public int getMax(int i, boolean odd) {
        return max;
    }

    /**
     * setter.
     *
     * @param max max
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * print sum.
     *
     * @param min min
     * @param max max
     * @param odd odd
     */
    public void printNumbers(int min, int max, boolean odd) {
        int sum = 0;
        for (int i = min; i < max; i++) {
            if (odd = false) {
                if (i % 2 == 0) {
                    System.out.println(i);
                    sum = sum + i;
                }
            } else if (i % 2 != 0) {
                System.out.println(i);
                sum = sum + i;

            }

        }
        System.out.println(sum);
    }

}
