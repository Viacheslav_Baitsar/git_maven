package com.viach.work;

/**
 * Class to work with Fibonachi.
 */
public class Fibonachi {

    /**
     * max percent.
     * qty
     */
    static final int MAX_PERCENT = 100;
    private int qtyOfFiboNumbers;

    /**
     * getter for MaxPercent.
     *
     * @return MaxPercent
     */
    public int getMaxPercent() {
        return MAX_PERCENT;
    }

    /**
     * getter for getQtyOfFiboNumbers.
     *
     * @return getQtyOfFiboNumbers
     */
    public int getQtyOfFiboNumbers() {
        return qtyOfFiboNumbers;
    }

    /**
     * @param qtyOfFiboNumbers qtyOfFiboNumbers.
     */
    public void setQtyOfFiboNumbers(int qtyOfFiboNumbers) {
        this.qtyOfFiboNumbers = qtyOfFiboNumbers;
    }

    /**
     * Constructor empty.
     */
    public Fibonachi() {
    }


    /**
     * @param sizeOfArray qty fibonachi numbers
     * @return array full of fibonachi numbers
     */
    public int[] generateFibonachiNumbers(int sizeOfArray) {
        int[] arrayOfFibonachiNumbers = new int[sizeOfArray];
        arrayOfFibonachiNumbers[0] = 0;
        arrayOfFibonachiNumbers[1] = 1;
        for (int i = 2; i < arrayOfFibonachiNumbers.length; i++) {
            arrayOfFibonachiNumbers[i] = arrayOfFibonachiNumbers[i - 1] + arrayOfFibonachiNumbers[i - 2];
        }
        return arrayOfFibonachiNumbers;
    }

    /**
     * @param arrayOfFibonachiNumbers array with fibonachi
     *                                print max
     *                                print qty
     *                                print %
     */
    public void percentageOfNumbers(int[] arrayOfFibonachiNumbers) {
    }

    /**
     * print max.
     *
     * @param arrayOfFibonachiNumbers arrayOfFibonachiNumbers
     * @param odd                     odd
     */
    public void max(int[] arrayOfFibonachiNumbers, boolean odd) {
        int max = 0;
        int qty = 0;
        for (int i = 1; i < arrayOfFibonachiNumbers.length; i++) {
            System.out.print(arrayOfFibonachiNumbers[i] + " ");
            if ((arrayOfFibonachiNumbers[i] % 2 == 0) == odd) {
                qty = qty + 1;
                if (max < arrayOfFibonachiNumbers[i]) {
                    max = arrayOfFibonachiNumbers[i];
                }
            }
        }
        System.out.println(max + " max");
        System.out.println(qty);
        System.out.println(arrayOfFibonachiNumbers.length);
        System.out.println(getMaxPercent()
                * qty / arrayOfFibonachiNumbers.length + " %%");
    }
}
