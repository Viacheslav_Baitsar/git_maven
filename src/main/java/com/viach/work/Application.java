package com.viach.work;

import java.util.Scanner;

/**
 * run app.
 */
public class Application {
    /**
     * Main.
     * @param args args
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        PrintOddEven fib = new PrintOddEven();
        String min = sc.nextLine();
        String max = sc.nextLine();
        String aoo = sc.nextLine();
        fib.setMin(Integer.parseInt(min));
        fib.setMax(Integer.parseInt(max));
        fib.setOdd(Boolean.getBoolean(aoo));
        fib.printNumbers(fib.min, fib.max, fib.odd);
        Fibonachi fib1 = new Fibonachi();
        fib1.setQtyOfFiboNumbers(Integer.parseInt(sc.nextLine()));
        fib1.max(fib1.generateFibonachiNumbers(fib1.getQtyOfFiboNumbers()), true);
    }
}
